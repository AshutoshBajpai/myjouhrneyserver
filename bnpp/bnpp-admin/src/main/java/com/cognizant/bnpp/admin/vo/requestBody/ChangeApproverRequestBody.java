package com.cognizant.bnpp.admin.vo.requestBody;

public class ChangeApproverRequestBody {
	private String employeeUid;
	private int empLeaveAppId;
	private String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getEmployeeUid() {
		return employeeUid;
	}

	public void setEmployeeUid(String employeeUid) {
		this.employeeUid = employeeUid;
	}

/*	public int getEmpLeaveApplicationId() {
		return empLeaveApplicationId;
	}

	public void setEmpLeaveApplicationId(int empLeaveApplicationId) {
		this.empLeaveApplicationId = empLeaveApplicationId;
	}*/
	
	public int getEmpLeaveAppId() {
		return empLeaveAppId;
	}

	public void setEmpLeaveAppId(int empLeaveAppId) {
		this.empLeaveAppId = empLeaveAppId;
	}

	public ChangeApproverRequestBody(String employeeUid, int empLeaveApplicationId, String action) {
		super();
		this.employeeUid = employeeUid;
		this.empLeaveAppId = empLeaveApplicationId;
		this.action = action;
	}


	public ChangeApproverRequestBody(String employeeUid, int empLeaveApplicationId) {
		super();
		this.employeeUid = employeeUid;
		this.empLeaveAppId = empLeaveApplicationId;
	}

	public ChangeApproverRequestBody() {
		super();
	}

}
