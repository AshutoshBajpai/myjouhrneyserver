package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppLeaveBiz;
import com.cognizant.bnpp.admin.entity.BnppLeave;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("las/rest/services/leaveApplication")
public class LeaveApplicationController extends BaseController<BnppLeaveBiz, BnppLeave> {

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	//public ObjectRestResponse<String> leaveRequest(HttpServletRequest hsRequest, @RequestParam(value = "param") String request)
	public ObjectRestResponse<?> leaveRequest(@RequestParam(value = "param") String request,
				@RequestPart(value = "file", required=false) MultipartFile file)	
			throws Exception {
		/*MultipartFile file = new MultipartFile;*/
		//MultipartFile file = ((MultipartHttpServletRequest) hsRequest).getFile("file");
		//List<MultipartFile> files = ((MultipartHttpServletRequest) hsRequest).getFiles("file");
		
		//MultipartFile file = files.get(0);
		
		if (file != null) {
			System.out.println("with attachment ");
		return baseBiz.leaveRequest(file, request);
		
		
		
		}else {
		//return baseBiz.leaveRequestWithoutDoc(request);
			System.out.println("without attachment ");
			return baseBiz.leaveRequest(file, request);
		}
	}
}