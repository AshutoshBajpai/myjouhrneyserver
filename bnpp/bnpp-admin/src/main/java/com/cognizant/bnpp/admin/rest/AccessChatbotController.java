package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppStaffBiz;
import com.cognizant.bnpp.admin.entity.BnppStaff;
import com.cognizant.bnpp.admin.vo.ChatbotInfo;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/accessChatbot")
public class AccessChatbotController extends BaseController<BnppStaffBiz, BnppStaff> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<ChatbotInfo> accessChatbot(@RequestParam("user_id") String user_id,
			@RequestParam("message") String message, @RequestParam("user_region") String user_region,
			@RequestParam("cat1") String cat1, @RequestParam("cat2") String cat2, @RequestParam("cat3") String cat3) {
		// TODO
		System.out.println("----accessChatbot-----");
		System.out.println("user_id:" + user_id + ",message:" + ",user_region:" + user_region + ",cat1:" + cat1
				+ ",cat2" + cat2 + ",cat3" + cat3);
		return baseBiz.accessChatbot(user_id, message, user_region, cat1, cat2, cat3);
	}

}