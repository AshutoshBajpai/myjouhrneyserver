package com.cognizant.bnpp.admin.vo;

import java.util.List;

import com.cognizant.bnpp.admin.entity.TeamMemberInfo;

public class TeamCalendarVo {
	private String date;
	private List<TeamMemberInfo> memberInfo;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<TeamMemberInfo> getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(List<TeamMemberInfo> memberInfo) {
		this.memberInfo = memberInfo;
	}

	public TeamCalendarVo() {
		super();
	}

	public TeamCalendarVo(String date, List<TeamMemberInfo> memberInfo) {
		super();
		this.date = date;
		this.memberInfo = memberInfo;
	}

}
