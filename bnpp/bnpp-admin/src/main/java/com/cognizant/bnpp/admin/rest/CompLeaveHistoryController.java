package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppHolidayWorkBiz;
import com.cognizant.bnpp.admin.entity.BnppHolidayWork;
import com.cognizant.bnpp.admin.vo.CompLeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/compLeaveHistory")
public class CompLeaveHistoryController extends BaseController<BnppHolidayWorkBiz, BnppHolidayWork> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<List<CompLeaveHistoryItemVo>> getLeaveHistory(
			@RequestBody LeaveHistoryRequestBody leaveHistoryRequestBody) throws Exception {
		return baseBiz.getCompLeaveHistoryList(leaveHistoryRequestBody);
	}
}