package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppStaffBiz;
import com.cognizant.bnpp.admin.entity.BnppStaff;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/CompLeaveBalance")
public class CompLeaveBalanceController extends BaseController<BnppStaffBiz, BnppStaff> {
	@RequestMapping(value = "/{empno}", method = RequestMethod.GET)
	@ResponseBody
	public ObjectRestResponse<?> getCompLeaveBalance(@PathVariable(value = "empno") String empno) throws Exception {
		return baseBiz.getCompLeaveBalance(empno);
	}
}