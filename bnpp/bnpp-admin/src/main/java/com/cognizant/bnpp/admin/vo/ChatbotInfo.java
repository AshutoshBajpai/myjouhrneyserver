package com.cognizant.bnpp.admin.vo;

public class ChatbotInfo {
	private String user_id;
	private String user_region;
	private String cat1;
	private String cat2;
	private String cat3;
	private String response;
	private String response_select;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_region() {
		return user_region;
	}

	public void setUser_region(String user_region) {
		this.user_region = user_region;
	}

	public String getCat1() {
		return cat1;
	}

	public void setCat1(String cat1) {
		this.cat1 = cat1;
	}

	public String getCat2() {
		return cat2;
	}

	public void setCat2(String cat2) {
		this.cat2 = cat2;
	}

	public String getCat3() {
		return cat3;
	}

	public void setCat3(String cat3) {
		this.cat3 = cat3;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponse_select() {
		return response_select;
	}

	public void setResponse_select(String response_select) {
		this.response_select = response_select;
	}

}
