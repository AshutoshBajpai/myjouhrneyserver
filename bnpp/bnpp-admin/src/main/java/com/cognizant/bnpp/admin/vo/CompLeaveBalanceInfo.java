package com.cognizant.bnpp.admin.vo;

public class CompLeaveBalanceInfo {
	private String primaryApprover;
	private String empNo;
	private String empSapName;
	private int daysApproved;
	private int daysConsumed;
	private int balance;

	public CompLeaveBalanceInfo() {
		super();
	}

	public String getPrimaryApprover() {
		return primaryApprover;
	}

	public void setPrimaryApprover(String primaryApprover) {
		this.primaryApprover = primaryApprover;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpSapName() {
		return empSapName;
	}

	public void setEmpSapName(String empSapName) {
		this.empSapName = empSapName;
	}

	public int getDaysApproved() {
		return daysApproved;
	}

	public void setDaysApproved(int daysApproved) {
		this.daysApproved = daysApproved;
	}

	public int getDaysConsumed() {
		return daysConsumed;
	}

	public void setDaysConsumed(int daysConsumed) {
		this.daysConsumed = daysConsumed;
	}

	public CompLeaveBalanceInfo(String primaryApprover, String empNo, String empSapName, int daysApproved,
			int daysConsumed, int balance) {
		super();
		this.primaryApprover = primaryApprover;
		this.empNo = empNo;
		this.empSapName = empSapName;
		this.daysApproved = daysApproved;
		this.daysConsumed = daysConsumed;
		this.balance = balance;
	}

}
