package com.cognizant.bnpp.admin.biz;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.bnpp.admin.corebackend.biz.BnppApiBiz;
import com.cognizant.bnpp.admin.corebackend.biz.ChatbotApiBiz;
import com.cognizant.bnpp.admin.vo.ChatbotInfo;
import com.cognizant.bnpp.admin.vo.CompPendingRequestVo;
import com.cognizant.bnpp.admin.vo.LeaveAppType;
import com.cognizant.bnpp.admin.vo.LeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.PendingRequestVo;
import com.cognizant.bnpp.admin.vo.requestBody.ApproveOrRejectRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.CancelLeaveRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.DocumentRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.MarkBLRequestBody;
import com.cognizant.bnpp.common.biz.BaseBiz;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

@Service
public class BnppStaffBiz extends BaseBiz {

	@Autowired
	private BnppApiBiz bnppApiBiz;
	@Autowired
	private ChatbotApiBiz chatbotApiBiz;

	public ObjectRestResponse<List<LeaveAppType>> getLeaveAppSetup(String empno) {
		return bnppApiBiz.getLeaveAppSetup(empno);
	}

	public ObjectRestResponse<String> reject(ApproveOrRejectRequestBody approveOrRejectRequestBody) {
		return bnppApiBiz.reject(approveOrRejectRequestBody);
	}

	public ObjectRestResponse<String> approve(ApproveOrRejectRequestBody approveOrRejectRequestBody) {
		return bnppApiBiz.approve(approveOrRejectRequestBody);
	}

	public ObjectRestResponse<List<CompPendingRequestVo>> getCompPendingRequest(String empno) {
		return bnppApiBiz.getCompPendingRequest(empno);
	}

	public ObjectRestResponse<List<PendingRequestVo>> getPendingRequestList(String empno) {
		return bnppApiBiz.getPendingRequest(empno);
	}

	public ObjectRestResponse<Integer> getPendingNumber(String empno) {
		return bnppApiBiz.getPendingNumber(empno);
	}

	public ObjectRestResponse<String> markBL(MarkBLRequestBody markBLRequestBody) {
		return bnppApiBiz.markBL(markBLRequestBody);
	}

	public ObjectRestResponse<List<LeaveHistoryItemVo>> getLeaveHistory(
			LeaveHistoryRequestBody leaveHistoryRequestBody) {
		return bnppApiBiz.getLeaveHistory(leaveHistoryRequestBody);
	}

	public ObjectRestResponse<?> getLeaveBalance(String empno) throws Exception {
		return bnppApiBiz.getLeaveBalance(empno);
	}

	public ObjectRestResponse<String> authorize(String empno) throws Exception {
		return bnppApiBiz.authorize(empno);
	}

	public ObjectRestResponse<String> cancelLeave(CancelLeaveRequestBody cancelLeaveRequestBody) {
		return bnppApiBiz.cancelLeave(cancelLeaveRequestBody);
	}

	public ObjectRestResponse<?> getCompLeaveBalance(String empno) {
		return bnppApiBiz.getCompLeaveBalance(empno);
	}

	public ObjectRestResponse<ChatbotInfo> accessChatbot(String user_id, String message, String user_region,
			String cat1, String cat2, String cat3) {
		return chatbotApiBiz.accessChatbotApi(user_id, message, user_region, cat1, cat2, cat3);
	}
	
	public ResponseEntity<byte[]> viewDoc(DocumentRequestBody documentRequestBody) {
	//public File viewDoc(DocumentRequestBody documentRequestBody) {	
	return bnppApiBiz.viewDoc(documentRequestBody);
	}
}