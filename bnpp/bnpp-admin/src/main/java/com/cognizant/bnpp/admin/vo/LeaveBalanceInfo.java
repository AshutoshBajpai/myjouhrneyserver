package com.cognizant.bnpp.admin.vo;

import java.util.List;

import com.cognizant.bnpp.admin.entity.BlockLeaveMember;

public class LeaveBalanceInfo {
	private int leaveEntitlement;
	private float leaveAdjusted;
	private int leaveConsumed;
	private String blockLeaveInd;
	private int leaveBroughtForward;
	private int leaveBalance;
	private String blockLeaveMsg;
	private String primaryApprover;
	private String backupApprover;
	private String blockLeaveColor;
	private String empNo;
	private String empSapName;
	private String primaryApproverID;
	private String backupApproverID;
	private NextLeave nextLeave;
	private List<BlockLeaveMember> blockLeaveTeam;
	private int compliantCount;
	private int nonCompliantCount;
	private int nonCompliantPendingCount;
	private int exemptCount;
	private int totalCount;

	public LeaveBalanceInfo() {
		super();
	}

	public String getPrimaryApprover() {
		return primaryApprover;
	}

	public void setPrimaryApprover(String primaryApprover) {
		this.primaryApprover = primaryApprover;
	}

	public String getBackupApprover() {
		return backupApprover;
	}

	public void setBackupApprover(String backupApprover) {
		this.backupApprover = backupApprover;
	}

	public String getPrimaryApproverID() {
		return primaryApproverID;
	}

	public void setPrimaryApproverID(String primaryApproverID) {
		this.primaryApproverID = primaryApproverID;
	}

	public String getBackupApproverID() {
		return backupApproverID;
	}

	public void setBackupApproverID(String backupApproverID) {
		this.backupApproverID = backupApproverID;
	}

	public NextLeave getNextLeave() {
		return nextLeave;
	}

	public void setNextLeave(NextLeave nextLeave) {
		this.nextLeave = nextLeave;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpSapName() {
		return empSapName;
	}

	public void setEmpSapName(String empSapName) {
		this.empSapName = empSapName;
	}

	public int getLeaveEntitlement() {
		return leaveEntitlement;
	}

	public void setLeaveEntitlement(int leaveEntitlement) {
		this.leaveEntitlement = leaveEntitlement;
	}

	public float getLeaveAdjusted() {
		return leaveAdjusted;
	}

	public void setLeaveAdjusted(float leaveAdjusted) {
		this.leaveAdjusted = leaveAdjusted;
	}

	public int getLeaveConsumed() {
		return leaveConsumed;
	}

	public void setLeaveConsumed(int leaveConsumed) {
		this.leaveConsumed = leaveConsumed;
	}

	public String getBlockLeaveInd() {
		return blockLeaveInd;
	}

	public void setBlockLeaveInd(String blockLeaveInd) {
		this.blockLeaveInd = blockLeaveInd;
	}

	public int getLeaveBroughtForward() {
		return leaveBroughtForward;
	}

	public void setLeaveBroughtForward(int leaveBroughtForward) {
		this.leaveBroughtForward = leaveBroughtForward;
	}

	public int getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	public String getBlockLeaveMsg() {
		return blockLeaveMsg;
	}

	public void setBlockLeaveMsg(String blockLeaveMsg) {
		this.blockLeaveMsg = blockLeaveMsg;
	}

	public String getBlockLeaveColor() {
		return blockLeaveColor;
	}

	public void setBlockLeaveColor(String blockLeaveColor) {
		this.blockLeaveColor = blockLeaveColor;
	}

	public int getCompliantCount() {
		return compliantCount;
	}

	public void setCompliantCount(int compliantCount) {
		this.compliantCount = compliantCount;
	}

	public int getNonCompliantCount() {
		return nonCompliantCount;
	}

	public void setNonCompliantCount(int nonCompliantCount) {
		this.nonCompliantCount = nonCompliantCount;
	}

	public int getNonCompliantPendingCount() {
		return nonCompliantPendingCount;
	}

	public void setNonCompliantPendingCount(int nonCompliantPendingCount) {
		this.nonCompliantPendingCount = nonCompliantPendingCount;
	}

	public int getExemptCount() {
		return exemptCount;
	}

	public void setExemptCount(int exemptCount) {
		this.exemptCount = exemptCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public List<BlockLeaveMember> getBlockLeaveTeam() {
		return blockLeaveTeam;
	}

	public void setBlockLeaveTeam(List<BlockLeaveMember> blockLeaveTeam) {
		this.blockLeaveTeam = blockLeaveTeam;
	}

	public LeaveBalanceInfo(int leaveEntitlement, float leaveAdjusted, int leaveConsumed, String blockLeaveInd,
			int leaveBroughtForward, int leaveBalance, String blockLeaveMsg, String primaryApprover,
			String backupApprover, String blockLeaveColor, String empNo, String empSapName, String primaryApproverID,
			String backupApproverID, NextLeave nextLeave) {
		super();
		this.leaveEntitlement = leaveEntitlement;
		this.leaveAdjusted = leaveAdjusted;
		this.leaveConsumed = leaveConsumed;
		this.blockLeaveInd = blockLeaveInd;
		this.leaveBroughtForward = leaveBroughtForward;
		this.leaveBalance = leaveBalance;
		this.blockLeaveMsg = blockLeaveMsg;
		this.primaryApprover = primaryApprover;
		this.backupApprover = backupApprover;
		this.blockLeaveColor = blockLeaveColor;
		this.empNo = empNo;
		this.empSapName = empSapName;
		this.primaryApproverID = primaryApproverID;
		this.backupApproverID = backupApproverID;
		this.nextLeave = nextLeave;
	}

	public LeaveBalanceInfo(int leaveEntitlement, float leaveAdjusted, int leaveConsumed, String blockLeaveInd,
			int leaveBroughtForward, int leaveBalance, String blockLeaveMsg, String primaryApprover,
			String backupApprover, String blockLeaveColor, String empNo, String empSapName, String primaryApproverID,
			String backupApproverID, NextLeave nextLeave, List<BlockLeaveMember> blockLeaveTeam, int compliantCount,
			int nonCompliantCount, int nonCompliantPendingCount, int exemptCount, int totalCount) {
		super();
		this.leaveEntitlement = leaveEntitlement;
		this.leaveAdjusted = leaveAdjusted;
		this.leaveConsumed = leaveConsumed;
		this.blockLeaveInd = blockLeaveInd;
		this.leaveBroughtForward = leaveBroughtForward;
		this.leaveBalance = leaveBalance;
		this.blockLeaveMsg = blockLeaveMsg;
		this.primaryApprover = primaryApprover;
		this.backupApprover = backupApprover;
		this.blockLeaveColor = blockLeaveColor;
		this.empNo = empNo;
		this.empSapName = empSapName;
		this.primaryApproverID = primaryApproverID;
		this.backupApproverID = backupApproverID;
		this.nextLeave = nextLeave;
		this.blockLeaveTeam = blockLeaveTeam;
		this.compliantCount = compliantCount;
		this.nonCompliantCount = nonCompliantCount;
		this.nonCompliantPendingCount = nonCompliantPendingCount;
		this.exemptCount = exemptCount;
		this.totalCount = totalCount;
	}

}
