package com.cognizant.bnpp.admin.vo;

public class LeaveApplicationResponse {
	
	private String status;
	private String message;
	private String popupMessage;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPopupMessage() {
		return popupMessage;
	}
	public void setPopupMessage(String popupMessage) {
		this.popupMessage = popupMessage;
	}
	

}
