package com.cognizant.bnpp.admin.vo.requestBody;

public class LeaveHistoryRequestBody {
	private String employeeUid;
	private String year;
	private String type;

	public String getEmployeeUid() {
		return employeeUid;
	}

	public void setEmployeeUid(String employeeUid) {
		this.employeeUid = employeeUid;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
