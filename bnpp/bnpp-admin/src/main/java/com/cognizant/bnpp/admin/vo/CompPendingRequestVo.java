package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class CompPendingRequestVo {
	private String employeeName;
	private int empLeaveApplicationID;
	private Date holidayDate;
	private float daysApplied;
	private String reason;
	private int leaveCategory;
	private String leaveStatus;
	private String rejectReasonInd;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmpLeaveApplicationID() {
		return empLeaveApplicationID;
	}

	public void setEmpLeaveApplicationID(int empLeaveApplicationID) {
		this.empLeaveApplicationID = empLeaveApplicationID;
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	public float getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(float daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getLeaveCategory() {
		return leaveCategory;
	}

	public void setLeaveCategory(int leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getRejectReasonInd() {
		return rejectReasonInd;
	}

	public void setRejectReasonInd(String rejectReasonInd) {
		this.rejectReasonInd = rejectReasonInd;
	}

	public CompPendingRequestVo() {
		super();
	}

	public CompPendingRequestVo(String employeeName, int empLeaveApplicationID, Date holidayDate, float daysApplied,
			String reason, int leaveCategory, String leaveStatus, String rejectReasonInd) {
		super();
		this.employeeName = employeeName;
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.holidayDate = holidayDate;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveCategory = leaveCategory;
		this.leaveStatus = leaveStatus;
		this.rejectReasonInd = rejectReasonInd;
	}

}
