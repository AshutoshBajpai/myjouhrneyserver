package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class PendingRequestVo {
	private String employeeName;
	private int empLeaveApplicationID;
	private Date dateFrom;
	private Date dateTo;
	private float daysApplied;
	private String reason;
	private String leaveType;
	private String leaveStatus;
	private int leaveCategory;
	private String leaveOptionFrom;
	private String leaveOptionTo;
	private String documentViewOptionInd;
	private String primaryApproverInd;
	private String rejectReasonInd;
	private String blockLeaveInd;
	//Ashutosh start
	private String backupApproverInd;
	private String approveReasonInd;
	//Ashutosh end

	public PendingRequestVo() {
		super();
	}

	public String getBlockLeaveInd() {
		return blockLeaveInd;
	}

	public void setBlockLeaveInd(String blockLeaveInd) {
		this.blockLeaveInd = blockLeaveInd;
	}

	public String getPrimaryApproverInd() {
		return primaryApproverInd;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setPrimaryApproverInd(String primaryApproverInd) {
		this.primaryApproverInd = primaryApproverInd;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmpLeaveApplicationID() {
		return empLeaveApplicationID;
	}

	public void setEmpLeaveApplicationID(int empLeaveApplicationID) {
		this.empLeaveApplicationID = empLeaveApplicationID;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public float getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(float daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public int getLeaveCategory() {
		return leaveCategory;
	}

	public void setLeaveCategory(int leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	public String getLeaveOptionFrom() {
		return leaveOptionFrom;
	}

	public void setLeaveOptionFrom(String leaveOptionFrom) {
		this.leaveOptionFrom = leaveOptionFrom;
	}

	public String getLeaveOptionTo() {
		return leaveOptionTo;
	}

	public void setLeaveOptionTo(String leaveOptionTo) {
		this.leaveOptionTo = leaveOptionTo;
	}

	public String getDocumentViewOptionInd() {
		return documentViewOptionInd;
	}

	public void setDocumentViewOptionInd(String documentViewOptionInd) {
		this.documentViewOptionInd = documentViewOptionInd;
	}

	public String getRejectReasonInd() {
		return rejectReasonInd;
	}

	public void setRejectReasonInd(String rejectReasonInd) {
		this.rejectReasonInd = rejectReasonInd;
	}
	
	
	//Ashutosh start
	public String getBackupApproverInd() {
		return backupApproverInd;
	}

	public void setBackupApproverInd(String backupApproverInd) {
		this.backupApproverInd = backupApproverInd;
	}
	
	public String getApproveReasonInd() {
		return approveReasonInd;
	}

	public void setApproveReasonInd(String approveReasonInd) {
		this.approveReasonInd = approveReasonInd;
	}
	
	//Ashutosh end



	public PendingRequestVo(String employeeName, int empLeaveApplicationID, Date dateFrom, Date dateTo,
			float daysApplied, String reason, String leaveType, String leaveStatus, int leaveCategory,
			String leaveOptionFrom, String leaveOptionTo, String documentViewOptionInd, String primaryApproverInd,
			String rejectReasonInd, String blockLeaveInd, String backupApproverInd, String approveReasonInd ) {
		super();
		this.employeeName = employeeName;
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveType = leaveType;
		this.leaveStatus = leaveStatus;
		this.leaveCategory = leaveCategory;
		this.leaveOptionFrom = leaveOptionFrom;
		this.leaveOptionTo = leaveOptionTo;
		this.documentViewOptionInd = documentViewOptionInd;
		this.primaryApproverInd = primaryApproverInd;
		this.rejectReasonInd = rejectReasonInd;
		this.blockLeaveInd = blockLeaveInd;
		//Ashutosh start
		this.backupApproverInd = backupApproverInd;
		this.approveReasonInd = approveReasonInd;
		//Ashutosh end
	}

}
