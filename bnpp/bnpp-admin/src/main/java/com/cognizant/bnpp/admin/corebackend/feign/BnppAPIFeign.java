package com.cognizant.bnpp.admin.corebackend.feign;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.cognizant.bnpp.admin.vo.CompLeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.CompPendingRequestVo;
import com.cognizant.bnpp.admin.vo.LeaveAppType;
import com.cognizant.bnpp.admin.vo.LeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.PendingRequestVo;
import com.cognizant.bnpp.admin.vo.TeamCalendarVo;
import com.cognizant.bnpp.admin.vo.requestBody.ApproveOrRejectRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.CancelLeaveRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.ChangeApproverRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.DocumentRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.HolidayWorkRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.MarkBLRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.TeamCalendarRequestBody;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

@FeignClient(url = "${corebackendurl}", name = "bnpp")
public interface BnppAPIFeign {
	// authorize
	@GetMapping(value = "authorize/{empno}")
	public ObjectRestResponse<String> authorize(@PathVariable("empno") String empno);

	// leave balance
	@GetMapping(value = "leaveBalance/{empno}")
	public ObjectRestResponse<?> getLeaveBalance(@PathVariable("empno") String empno);

	// comp leave balance
	@GetMapping(value = "CompLeaveBalance/{empno}")
	public ObjectRestResponse<?> getCompLeaveBalance(@PathVariable("empno") String empno);

	// team calendar
	@PostMapping(value = "teamCalendar")
	public ObjectRestResponse<List<TeamCalendarVo>> getTeamCalendar(
			@RequestBody TeamCalendarRequestBody teamCalendarRequestBody);

	// leave history
	@PostMapping(value = "leaveHistory")
	public ObjectRestResponse<List<LeaveHistoryItemVo>> getLeaveHistory(
			@RequestBody LeaveHistoryRequestBody leaveHistoryRequestBody);

	// comp leave history
	@PostMapping(value = "compLeaveHistory")
	public ObjectRestResponse<List<CompLeaveHistoryItemVo>> getCompLeaveHistory(
			@RequestBody LeaveHistoryRequestBody leaveHistoryRequestBody);

	// cancel leave
	@PostMapping(value = "cancelLeave")
	public ObjectRestResponse<String> cancelLeave(@RequestBody CancelLeaveRequestBody cancelLeaveRequestBody);

	// change approver
	@PostMapping(value = "changeApprover")
	public ObjectRestResponse<String> changeApprover(@RequestBody ChangeApproverRequestBody changeApproverRequestBody);

	// mark block leave
	@PostMapping(value = "markBL")
	public ObjectRestResponse<String> markBL(@RequestBody MarkBLRequestBody markBLRequestBody);

	// pending num
	@GetMapping(value = "pendingNumber/{empno}")
	public ObjectRestResponse<Integer> getPendingNumber(@PathVariable(value = "empno") String empno);

	// pending request
	@GetMapping(value = "pendingLeaveRequest/{empno}")
	public ObjectRestResponse<List<PendingRequestVo>> getPendingRequest(@PathVariable(value = "empno") String empno);

	// comp pending request
	@GetMapping(value = "pendingCompLeaveRequest/{empno}")
	public ObjectRestResponse<List<CompPendingRequestVo>> getCompPendingRequest(
			@PathVariable(value = "empno") String empno);

	// approve
	@PostMapping(value = "approve")
	public ObjectRestResponse<String> approve(@RequestBody ApproveOrRejectRequestBody approveOrRejectRequestBody);

	// reject
	@PostMapping(value = "reject")
	public ObjectRestResponse<String> reject(@RequestBody ApproveOrRejectRequestBody approveOrRejectRequestBody);

	// leave app setup
	@GetMapping(value = "leaveApplicationSetup/{empno}")
	public ObjectRestResponse<List<LeaveAppType>> getLeaveAppSetup(@PathVariable(value = "empno") String empno);

	// comp leave application
	@PostMapping(value = "compLeaveApplication")
	public ObjectRestResponse<String> holidayWorkRequest(@RequestBody HolidayWorkRequestBody holidayWorkRequestBody);

	// leave application
	@PostMapping(value = "leaveApplication")
	public ObjectRestResponse<?> leaveRequest(@RequestBody LeaveRequestBody leaveRequest);
	
	// viewDoc
	@PostMapping(value = "viewDoc")
	public ResponseEntity<byte[]> viewDoc(@RequestBody DocumentRequestBody documentRequestBody);
	//public HttpServletResponse viewDoc(@RequestBody DocumentRequestBody documentRequestBody);

}
