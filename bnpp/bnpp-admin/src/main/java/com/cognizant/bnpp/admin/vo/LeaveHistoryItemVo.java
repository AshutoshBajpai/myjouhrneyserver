package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class LeaveHistoryItemVo {
	private int empLeaveApplicationID;
	private Date dateFrom;
	private Date dateTo;
	private int daysApplied;
	private String reason;
	private String leaveType;
	private String primaryApprovalName;
	private String backupApprovalName;
	private String leaveStatus;
	private String documentViewOptionInd;
	private String cancellationAllowed;
	private String cancelReasonInd;
	private String blockLeaveEligible;
	private String leaveOptionFrom;
	private String leaveOptionTo;
	private String activatePlanAllowed;
	private String blockLeaveTagged;

	public String getBlockLeaveTagged() {
		return blockLeaveTagged;
	}

	public void setBlockLeaveTagged(String blockLeaveTagged) {
		this.blockLeaveTagged = blockLeaveTagged;
	}

	public String getActivatePlanAllowed() {
		return activatePlanAllowed;
	}

	public void setActivatePlanAllowed(String activatePlanAllowed) {
		this.activatePlanAllowed = activatePlanAllowed;
	}

	public String getChangeApproverAllowed() {
		return changeApproverAllowed;
	}

	public void setChangeApproverAllowed(String changeApproverAllowed) {
		this.changeApproverAllowed = changeApproverAllowed;
	}

	public String getDocumentUploadOptionInd() {
		return documentUploadOptionInd;
	}

	public void setDocumentUploadOptionInd(String documentUploadOptionInd) {
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

	private String changeApproverAllowed;
	private String documentUploadOptionInd;

	public int getEmpLeaveApplicationID() {
		return empLeaveApplicationID;
	}

	public void setEmpLeaveApplicationID(int empLeaveApplicationID) {
		this.empLeaveApplicationID = empLeaveApplicationID;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public int getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(int daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getPrimaryApprovalName() {
		return primaryApprovalName;
	}

	public void setPrimaryApprovalName(String primaryApprovalName) {
		this.primaryApprovalName = primaryApprovalName;
	}

	public String getBackupApprovalName() {
		return backupApprovalName;
	}

	public void setBackupApprovalName(String backupApprovalName) {
		this.backupApprovalName = backupApprovalName;
	}

	public String getDocumentViewOptionInd() {
		return documentViewOptionInd;
	}

	public void setDocumentViewOptionInd(String documentViewOptionInd) {
		this.documentViewOptionInd = documentViewOptionInd;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getCancellationAllowed() {
		return cancellationAllowed;
	}

	public void setCancellationAllowed(String cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	public String getCancelReasonInd() {
		return cancelReasonInd;
	}

	public void setCancelReasonInd(String cancelReasonInd) {
		this.cancelReasonInd = cancelReasonInd;
	}

	public String getBlockLeaveEligible() {
		return blockLeaveEligible;
	}

	public void setBlockLeaveEligible(String blockLeaveEligible) {
		this.blockLeaveEligible = blockLeaveEligible;
	}

	public String getLeaveOptionFrom() {
		return leaveOptionFrom;
	}

	public void setLeaveOptionFrom(String leaveOptionFrom) {
		this.leaveOptionFrom = leaveOptionFrom;
	}

	public String getLeaveOptionTo() {
		return leaveOptionTo;
	}

	public void setLeaveOptionTo(String leaveOptionTo) {
		this.leaveOptionTo = leaveOptionTo;
	}

	public LeaveHistoryItemVo(int empLeaveApplicationID, Date dateFrom, Date dateTo, int daysApplied, String reason,
			String leaveType, String primaryApprovalName, String backupApprovalName, String leaveStatus,
			String documentViewOptionInd, String cancellationAllowed, String cancelReasonInd, String blockLeaveEligible,
			String leaveOptionFrom, String leaveOptionTo, String activatePlanAllowed, String changeApproverAllowed,
			String documentUploadOptionInd) {
		super();
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveType = leaveType;
		this.primaryApprovalName = primaryApprovalName;
		this.backupApprovalName = backupApprovalName;
		this.leaveStatus = leaveStatus;
		this.documentViewOptionInd = documentViewOptionInd;
		this.cancellationAllowed = cancellationAllowed;
		this.cancelReasonInd = cancelReasonInd;
		this.blockLeaveEligible = blockLeaveEligible;
		this.leaveOptionFrom = leaveOptionFrom;
		this.leaveOptionTo = leaveOptionTo;
		this.activatePlanAllowed = activatePlanAllowed;
		this.changeApproverAllowed = changeApproverAllowed;
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

	public LeaveHistoryItemVo() {
		super();
	}

	public LeaveHistoryItemVo(int empLeaveApplicationID, Date dateFrom, Date dateTo, int daysApplied, String reason,
			String leaveType, String primaryApprovalName, String backupApprovalName, String leaveStatus,
			String documentViewOptionInd, String cancellationAllowed, String cancelReasonInd, String blockLeaveEligible,
			String leaveOptionFrom, String leaveOptionTo) {
		super();
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveType = leaveType;
		this.primaryApprovalName = primaryApprovalName;
		this.backupApprovalName = backupApprovalName;
		this.leaveStatus = leaveStatus;
		this.documentViewOptionInd = documentViewOptionInd;
		this.cancellationAllowed = cancellationAllowed;
		this.cancelReasonInd = cancelReasonInd;
		this.blockLeaveEligible = blockLeaveEligible;
		this.leaveOptionFrom = leaveOptionFrom;
		this.leaveOptionTo = leaveOptionTo;
	}

	public LeaveHistoryItemVo(int empLeaveApplicationID, Date dateFrom, Date dateTo, int daysApplied, String reason,
			String leaveType, String primaryApprovalName, String backupApprovalName, String leaveStatus,
			String documentViewOptionInd, String cancellationAllowed, String cancelReasonInd, String blockLeaveEligible,
			String leaveOptionFrom, String leaveOptionTo, String activatePlanAllowed, String blockLeaveTagged,
			String changeApproverAllowed, String documentUploadOptionInd) {
		super();
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveType = leaveType;
		this.primaryApprovalName = primaryApprovalName;
		this.backupApprovalName = backupApprovalName;
		this.leaveStatus = leaveStatus;
		this.documentViewOptionInd = documentViewOptionInd;
		this.cancellationAllowed = cancellationAllowed;
		this.cancelReasonInd = cancelReasonInd;
		this.blockLeaveEligible = blockLeaveEligible;
		this.leaveOptionFrom = leaveOptionFrom;
		this.leaveOptionTo = leaveOptionTo;
		this.activatePlanAllowed = activatePlanAllowed;
		this.blockLeaveTagged = blockLeaveTagged;
		this.changeApproverAllowed = changeApproverAllowed;
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

}
