package com.cognizant.bnpp.admin.corebackend.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cognizant.bnpp.admin.vo.ChatbotInfo;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

@FeignClient(url = "${chatboturl}", name = "Chatbot")
public interface ChatbotAPIFeign {
	@PostMapping(value = "chatbot_api.py")
	public ObjectRestResponse<ChatbotInfo> accessChatbotApi(@RequestParam("user_id") String user_id,
			@RequestParam("message") String message, @RequestParam("user_region") String user_region,
			@RequestParam("cat1") String cat1, @RequestParam("cat2") String cat2, @RequestParam("cat3") String cat3);
}
