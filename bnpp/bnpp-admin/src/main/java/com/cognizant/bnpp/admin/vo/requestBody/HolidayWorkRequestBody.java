package com.cognizant.bnpp.admin.vo.requestBody;

public class HolidayWorkRequestBody {
	private String empNo;
	private String holidayDate;
	private float daysApplied;
	private String reason;
	private String action;

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	public float getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(float daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
