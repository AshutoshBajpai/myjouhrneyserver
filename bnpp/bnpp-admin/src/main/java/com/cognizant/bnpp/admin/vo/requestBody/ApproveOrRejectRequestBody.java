package com.cognizant.bnpp.admin.vo.requestBody;

public class ApproveOrRejectRequestBody {
	private String employeeUid;
	private int empLeaveApplicationId;
	private int leavecategory;
	private String reason;
	private String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public ApproveOrRejectRequestBody() {
		super();
	}

	public String getEmployeeUid() {
		return employeeUid;
	}

	public void setEmployeeUid(String employeeUid) {
		this.employeeUid = employeeUid;
	}

	public int getEmpLeaveApplicationId() {
		return empLeaveApplicationId;
	}

	public void setEmpLeaveApplicationId(int empLeaveApplicationId) {
		this.empLeaveApplicationId = empLeaveApplicationId;
	}

	public int getLeavecategory() {
		return leavecategory;
	}

	public void setLeavecategory(int leavecategory) {
		this.leavecategory = leavecategory;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public ApproveOrRejectRequestBody(String employeeUid, int empLeaveApplicationId, int leavecategory, String reason) {
		super();
		this.employeeUid = employeeUid;
		this.empLeaveApplicationId = empLeaveApplicationId;
		this.leavecategory = leavecategory;
		this.reason = reason;
	}

	public ApproveOrRejectRequestBody(String employeeUid, int empLeaveApplicationId, int leavecategory, String reason,
			String action) {
		super();
		this.employeeUid = employeeUid;
		this.empLeaveApplicationId = empLeaveApplicationId;
		this.leavecategory = leavecategory;
		this.reason = reason;
		this.action = action;
	}

}
