package com.cognizant.bnpp.admin.vo.requestBody;

public class LeaveRequestBody {
	private String dateFrom;
	private String dateTo;
	private String leaveOptionFrom;
	private String leaveOptionTo;
	private String reason;
	private String leaveType;
	private String empNo;
	private String primaryApprovalID;
	private String backupApprovalID;
	private String action;
	private int empLeaveApplicationID;

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getLeaveOptionFrom() {
		return leaveOptionFrom;
	}

	public void setLeaveOptionFrom(String leaveOptionFrom) {
		this.leaveOptionFrom = leaveOptionFrom;
	}

	public String getLeaveOptionTo() {
		return leaveOptionTo;
	}

	public void setLeaveOptionTo(String leaveOptionTo) {
		this.leaveOptionTo = leaveOptionTo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}



	public String getPrimaryApprovalID() {
		return primaryApprovalID;
	}

	public void setPrimaryApprovalID(String primaryApprovalID) {
		this.primaryApprovalID = primaryApprovalID;
	}

	public String getBackupApprovalID() {
		return backupApprovalID;
	}

	public void setBackupApprovalID(String backupApprovalID) {
		this.backupApprovalID = backupApprovalID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getEmpLeaveApplicationID() {
		return empLeaveApplicationID;
	}

	public void setEmpLeaveApplicationID(int empLeaveApplicationID) {
		this.empLeaveApplicationID = empLeaveApplicationID;
	}

}
