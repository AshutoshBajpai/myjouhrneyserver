package com.cognizant.bnpp.admin.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 加班申请及员工联合表
 * 
 * @author Likui.Mao
 * @email Likui.Mao@cognizant.com
 * @date 2018-10-10 18:57:27
 */
public class BnppHolidayWork implements Serializable {
	private static final long serialVersionUID = 1L;

	// 加班编号
	private Integer allowanceId;

	// 申请日
	private Float daysApplied;

	// 已用假日
	private Float daysConsumed;

	// 原因
	private String reason;

	// 加班申请状态
	private String applStatus;

	// 假日
	private Date holidayDate;

	// 有效起始
	private Date validFrom;

	// 有效截至
	private Date validTill;

	// 扩展有效性
	private String extensionAllowed;

	// 取消按钮
	private String cancellationAllowed;

	// 交换approver
	private String changeApproverAllowed;

	// 员工身份编号
	private String empNo;

	// 用户行为
	private String action;

	// 拒绝原因
	private String rejectReason;

	// 主签人
	private String primaryApprovalName;

	// 代签人
	private String backupApprovalName;

	// 假期类别
	private int leaveCategory;

	// 拒绝标志位
	private String rejectReasonInd;

	public int getLeaveCategory() {
		return leaveCategory;
	}

	public void setLeaveCategory(int leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	public String getRejectReasonInd() {
		return rejectReasonInd;
	}

	public void setRejectReasonInd(String rejectReasonInd) {
		this.rejectReasonInd = rejectReasonInd;
	}

	public String getPrimaryApprovalName() {
		return primaryApprovalName;
	}

	public void setPrimaryApprovalName(String primaryApprovalName) {
		this.primaryApprovalName = primaryApprovalName;
	}

	public String getBackupApprovalName() {
		return backupApprovalName;
	}

	public void setBackupApprovalName(String backupApprovalName) {
		this.backupApprovalName = backupApprovalName;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	/**
	 * 设置：加班编号
	 */
	public void setAllowanceId(Integer allowanceId) {
		this.allowanceId = allowanceId;
	}

	/**
	 * 获取：加班编号
	 */
	public Integer getAllowanceId() {
		return allowanceId;
	}

	/**
	 * 设置：申请日
	 */
	public void setDaysApplied(Float daysApplied) {
		this.daysApplied = daysApplied;
	}

	/**
	 * 获取：申请日
	 */
	public Float getDaysApplied() {
		return daysApplied;
	}

	/**
	 * 设置：已用假日
	 */
	public void setDaysConsumed(Float daysConsumed) {
		this.daysConsumed = daysConsumed;
	}

	/**
	 * 获取：已用假日
	 */
	public Float getDaysConsumed() {
		return daysConsumed;
	}

	/**
	 * 设置：原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 获取：原因
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * 设置：加班申请状态
	 */
	public void setApplStatus(String applStatus) {
		this.applStatus = applStatus;
	}

	/**
	 * 获取：加班申请状态
	 */
	public String getApplStatus() {
		return applStatus;
	}

	/**
	 * 设置：假日
	 */
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	/**
	 * 获取：假日
	 */
	public Date getHolidayDate() {
		return holidayDate;
	}

	/**
	 * 设置：有效起始
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * 获取：有效起始
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * 设置：有效截至
	 */
	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	/**
	 * 获取：有效截至
	 */
	public Date getValidTill() {
		return validTill;
	}

	/**
	 * 设置：扩展有效性
	 */
	public void setExtensionAllowed(String extensionAllowed) {
		this.extensionAllowed = extensionAllowed;
	}

	/**
	 * 获取：扩展有效性
	 */
	public String getExtensionAllowed() {
		return extensionAllowed;
	}

	/**
	 * 设置：取消按钮
	 */
	public void setCancellationAllowed(String cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	/**
	 * 获取：取消按钮
	 */
	public String getCancellationAllowed() {
		return cancellationAllowed;
	}

	/**
	 * 设置：交换approver
	 */
	public void setChangeApproverAllowed(String changeApproverAllowed) {
		this.changeApproverAllowed = changeApproverAllowed;
	}

	/**
	 * 获取：交换approver
	 */
	public String getChangeApproverAllowed() {
		return changeApproverAllowed;
	}

	/**
	 * 设置：员工身份编号
	 */
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	/**
	 * 获取：员工身份编号
	 */
	public String getEmpNo() {
		return empNo;
	}

	/**
	 * 设置：用户行为
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * 获取：用户行为
	 */
	public String getAction() {
		return action;
	}
}
