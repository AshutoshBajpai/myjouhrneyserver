package com.cognizant.bnpp.admin.entity;

import java.io.Serializable;

public class BlockLeaveMember implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String blockLeaveMsg;
	private String blockLeaveColor;
	private String empNo;

	public String getBlockLeaveMsg() {
		return blockLeaveMsg;
	}

	public void setBlockLeaveMsg(String blockLeaveMsg) {
		this.blockLeaveMsg = blockLeaveMsg;
	}

	public String getBlockLeaveColor() {
		return blockLeaveColor;
	}

	public void setBlockLeaveColor(String blockLeaveColor) {
		this.blockLeaveColor = blockLeaveColor;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public BlockLeaveMember() {
		super();
	}

	public BlockLeaveMember(String blockLeaveMsg, String blockLeaveColor, String empNo) {
		super();
		this.blockLeaveMsg = blockLeaveMsg;
		this.blockLeaveColor = blockLeaveColor;
		this.empNo = empNo;
	}
}
