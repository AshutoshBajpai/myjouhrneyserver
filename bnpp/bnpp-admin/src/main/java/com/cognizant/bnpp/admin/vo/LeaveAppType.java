package com.cognizant.bnpp.admin.vo;

import java.io.Serializable;

public class LeaveAppType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int leaveTypeID;
	private String leaveTypeDesc;
	private String msgDesc;
	private String requiredReasonInd;
	private String documentUploadOptionInd;
	private String documentUploadValidateInd;

	public LeaveAppType() {
		super();
	}

	public int getLeaveTypeID() {
		return leaveTypeID;
	}

	public void setLeaveTypeID(int leaveTypeID) {
		this.leaveTypeID = leaveTypeID;
	}

	public String getLeaveTypeDesc() {
		return leaveTypeDesc;
	}

	public void setLeaveTypeDesc(String leaveTypeDesc) {
		this.leaveTypeDesc = leaveTypeDesc;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public String getRequiredReasonInd() {
		return requiredReasonInd;
	}

	public void setRequiredReasonInd(String requiredReasonInd) {
		this.requiredReasonInd = requiredReasonInd;
	}

	public String getDocumentUploadOptionInd() {
		return documentUploadOptionInd;
	}

	public void setDocumentUploadOptionInd(String documentUploadOptionInd) {
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

	public String getDocumentUploadValidateInd() {
		return documentUploadValidateInd;
	}

	public void setDocumentUploadValidateInd(String documentUploadValidateInd) {
		this.documentUploadValidateInd = documentUploadValidateInd;
	}

	public LeaveAppType(int leaveTypeID, String leaveTypeDesc, String msgDesc, String requiredReasonInd,
			String documentUploadOptionInd, String documentUploadValidateInd) {
		super();
		this.leaveTypeID = leaveTypeID;
		this.leaveTypeDesc = leaveTypeDesc;
		this.msgDesc = msgDesc;
		this.requiredReasonInd = requiredReasonInd;
		this.documentUploadOptionInd = documentUploadOptionInd;
		this.documentUploadValidateInd = documentUploadValidateInd;
	}

}
