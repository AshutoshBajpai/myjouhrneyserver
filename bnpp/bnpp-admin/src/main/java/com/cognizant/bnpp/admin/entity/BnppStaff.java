package com.cognizant.bnpp.admin.entity;

import java.io.Serializable;

/**
 * 员工表
 * 
 * @author Likui.Mao
 * @email Likui.Mao@cognizant.com
 * @date 2018-09-26 15:25:00
 */
public class BnppStaff implements Serializable {
	private static final long serialVersionUID = 1L;

	// 员工身份编号
	private String empNo;

	// 员工姓名
	private String empName;

	// 法定假日
	private Integer leaveEntitlement;

	// 调休假日
	private Float leaveAdjusted;

	// 已休假日
	private Integer leaveConsumed;

	// 强制休假标记
	private String blockLeaveInd;

	// 提前请假天数
	private Integer leaveBroughtForward;

	// 剩余假日
	private Integer leaveBalance;

	// 强制休假信息
	private String blockLeaveMsg;

	// 强制休假色值
	private String blockLeaveColor;

	// 批准人
	private String level1ApprovalName;

	// 代理批准人
	private String level1BackupApprovalName;

	// 角色(0:??,1:normal,2:approve manager)
	private Integer role;

	private Integer daysApproved;

	public Integer getDaysApproved() {
		return daysApproved;
	}

	public void setDaysApproved(Integer daysApproved) {
		this.daysApproved = daysApproved;
	}

	public Integer getDaysConsumed() {
		return daysConsumed;
	}

	public void setDaysConsumed(Integer daysConsumed) {
		this.daysConsumed = daysConsumed;
	}

	public Integer getCompBalance() {
		return compBalance;
	}

	public void setCompBalance(Integer compBalance) {
		this.compBalance = compBalance;
	}

	private Integer daysConsumed;
	private Integer compBalance;

	/**
	 * 设置：员工身份编号
	 */
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	/**
	 * 获取：员工身份编号
	 */
	public String getEmpNo() {
		return empNo;
	}

	/**
	 * 设置：员工姓名
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * 获取：员工姓名
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * 设置：法定假日
	 */
	public void setLeaveEntitlement(Integer leaveEntitlement) {
		this.leaveEntitlement = leaveEntitlement;
	}

	/**
	 * 获取：法定假日
	 */
	public Integer getLeaveEntitlement() {
		return leaveEntitlement;
	}

	/**
	 * 设置：调休假日
	 */
	public void setLeaveAdjusted(Float leaveAdjusted) {
		this.leaveAdjusted = leaveAdjusted;
	}

	/**
	 * 获取：调休假日
	 */
	public Float getLeaveAdjusted() {
		return leaveAdjusted;
	}

	/**
	 * 设置：已休假日
	 */
	public void setLeaveConsumed(Integer leaveConsumed) {
		this.leaveConsumed = leaveConsumed;
	}

	/**
	 * 获取：已休假日
	 */
	public Integer getLeaveConsumed() {
		return leaveConsumed;
	}

	/**
	 * 设置：强制休假标记
	 */
	public void setBlockLeaveInd(String blockLeaveInd) {
		this.blockLeaveInd = blockLeaveInd;
	}

	/**
	 * 获取：强制休假标记
	 */
	public String getBlockLeaveInd() {
		return blockLeaveInd;
	}

	/**
	 * 设置：提前请假天数
	 */
	public void setLeaveBroughtForward(Integer leaveBroughtForward) {
		this.leaveBroughtForward = leaveBroughtForward;
	}

	/**
	 * 获取：提前请假天数
	 */
	public Integer getLeaveBroughtForward() {
		return leaveBroughtForward;
	}

	/**
	 * 设置：剩余假日
	 */
	public void setLeaveBalance(Integer leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	/**
	 * 获取：剩余假日
	 */
	public Integer getLeaveBalance() {
		return leaveBalance;
	}

	/**
	 * 设置：强制休假信息
	 */
	public void setBlockLeaveMsg(String blockLeaveMsg) {
		this.blockLeaveMsg = blockLeaveMsg;
	}

	/**
	 * 获取：强制休假信息
	 */
	public String getBlockLeaveMsg() {
		return blockLeaveMsg;
	}

	/**
	 * 设置：强制休假色值
	 */
	public void setBlockLeaveColor(String blockLeaveColor) {
		this.blockLeaveColor = blockLeaveColor;
	}

	/**
	 * 获取：强制休假色值
	 */
	public String getBlockLeaveColor() {
		return blockLeaveColor;
	}

	/**
	 * 设置：批准人
	 */
	public void setLevel1ApprovalName(String level1ApprovalName) {
		this.level1ApprovalName = level1ApprovalName;
	}

	/**
	 * 获取：批准人
	 */
	public String getLevel1ApprovalName() {
		return level1ApprovalName;
	}

	/**
	 * 设置：代理批准人
	 */
	public void setLevel1BackupApprovalName(String level1BackupApprovalName) {
		this.level1BackupApprovalName = level1BackupApprovalName;
	}

	/**
	 * 获取：代理批准人
	 */
	public String getLevel1BackupApprovalName() {
		return level1BackupApprovalName;
	}

	/**
	 * 设置：角色(0:??,1:normal,2:approve manager)
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * 获取：角色(0:??,1:normal,2:approve manager)
	 */
	public Integer getRole() {
		return role;
	}
}
