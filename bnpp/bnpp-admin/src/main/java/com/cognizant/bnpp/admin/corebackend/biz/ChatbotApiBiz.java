package com.cognizant.bnpp.admin.corebackend.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cognizant.bnpp.admin.corebackend.feign.ChatbotAPIFeign;
import com.cognizant.bnpp.admin.vo.ChatbotInfo;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

import feign.FeignException;

@Service
public class ChatbotApiBiz {
	@Autowired
	ChatbotAPIFeign chatbotAPIFeign;

	public ObjectRestResponse<ChatbotInfo> accessChatbotApi(String user_id, String message, String user_region,
			String cat1, String cat2, String cat3) {
		ObjectRestResponse<ChatbotInfo> response = new ObjectRestResponse<ChatbotInfo>();

		try {
			System.out.println("----------feign accessChatbotApi--------------");
			response = chatbotAPIFeign.accessChatbotApi(user_id, message, user_region, cat1, cat2, cat3);
			
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e:" + e.toString());
		}
		return response;
	}
}
