package com.cognizant.bnpp.admin.vo.requestBody;

public class MarkBLRequestBody {
	private String employeeUid;
	private String blockLeaveMap;
	private String remarks;
	private String year;

	public String getEmployeeUid() {
		return employeeUid;
	}

	public void setEmployeeUid(String employeeUid) {
		this.employeeUid = employeeUid;
	}

	public String getBlockLeaveMap() {
		return blockLeaveMap;
	}

	public void setBlockLeaveMap(String blockLeaveMap) {
		this.blockLeaveMap = blockLeaveMap;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public MarkBLRequestBody(String employeeUid, String blockLeaveMap, String remarks, String year) {
		super();
		this.employeeUid = employeeUid;
		this.blockLeaveMap = blockLeaveMap;
		this.remarks = remarks;
		this.year = year;
	}

	public MarkBLRequestBody() {
		super();
	}

}
