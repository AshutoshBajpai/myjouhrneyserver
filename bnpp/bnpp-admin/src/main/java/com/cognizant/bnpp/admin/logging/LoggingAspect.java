package com.cognizant.bnpp.admin.logging;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Pointcut("execution(* com.cognizant.bnpp.admin.rest.*.*(..)) ||"
			+ "execution(* com.cognizant.bnpp.admin.biz.*.*(..)) ||"
			+ "execution(* com.cognizant.bnpp.admin.corebackend.biz.*.*(..)) ||"
			+ "execution(* com.cognizant.bnpp.admin.corebackend.feign.*.*(..))")
	public void executionService() {

	}

	@Before("executionService()")
	public void logBefore(JoinPoint joinPoint) {
		logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
		logger.info("Beginning method: " + joinPoint.getSignature().getName());
		logger.info("Method arguments: " + Arrays.toString(joinPoint.getArgs()));
		logger.info("Method start at: " + System.currentTimeMillis());
	}

	@After("executionService()")
	public void logAfter(JoinPoint joinPoint) {
		logger.info("Method end at: " + System.currentTimeMillis());
		logger.info("Ending method: " + joinPoint.getSignature().getName());
	}

	@AfterThrowing(pointcut = "executionService()", throwing = "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
		logger.error("Beginning method: " + joinPoint.getSignature().getName());
		logger.error("Method arguments: " + Arrays.toString(joinPoint.getArgs()));
		logger.error("Exception in method: " + joinPoint.getSignature().getName());
		logger.error("Exception is: " + error);
	}
}