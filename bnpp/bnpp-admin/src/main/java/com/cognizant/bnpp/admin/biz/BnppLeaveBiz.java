package com.cognizant.bnpp.admin.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.bnpp.admin.corebackend.biz.BnppApiBiz;
import com.cognizant.bnpp.admin.vo.TeamCalendarVo;
import com.cognizant.bnpp.admin.vo.requestBody.ChangeApproverRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.TeamCalendarRequestBody;
import com.cognizant.bnpp.common.biz.BaseBiz;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

@Service
public class BnppLeaveBiz extends BaseBiz {
	@Autowired
	private BnppApiBiz bnppApiBiz;

	public ObjectRestResponse<String> changeApprover(ChangeApproverRequestBody changeApproverRequestBody) {
		return bnppApiBiz.changeApprover(changeApproverRequestBody);
	}

	public ObjectRestResponse<List<TeamCalendarVo>> getTeamCalendar(TeamCalendarRequestBody teamCalendarRequestBody)
			throws Exception {
		return bnppApiBiz.getTeamCalendar(teamCalendarRequestBody);
	}

	public ObjectRestResponse<?> leaveRequest(MultipartFile srcFile, String param) throws Exception {
		return bnppApiBiz.leaveRequest(param, srcFile);
	}
	
	public ObjectRestResponse<?> leaveRequestWithoutDoc(String param) throws Exception {
		return bnppApiBiz.leaveRequestWithoutDoc(param);
	}
}