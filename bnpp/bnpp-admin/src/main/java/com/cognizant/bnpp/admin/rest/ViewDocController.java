package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppStaffBiz;
import com.cognizant.bnpp.admin.entity.BnppStaff;
import com.cognizant.bnpp.admin.vo.requestBody.DocumentRequestBody;
import com.cognizant.bnpp.common.rest.BaseController;

import feign.Response;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/viewDoc")
public class ViewDocController extends BaseController<BnppStaffBiz, BnppStaff> {
	@Autowired
    private ServletContext servletContext;
	
	@RequestMapping(value = "/{empno}/{leaveApplicationId}", method = RequestMethod.GET
			, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	@ResponseBody
	public void viewDoc(@PathVariable(value = "empno") String empno,
			@PathVariable(value = "leaveApplicationId") int leaveApplicationId, 
			HttpServletResponse response)
			throws ServletException, IOException {
		
		
		DocumentRequestBody documentRequestBody = new DocumentRequestBody();
		documentRequestBody.setEmpNo(empno);
		documentRequestBody.setEmpLeaveApplicationId(leaveApplicationId);
		//return baseBiz.viewDoc(documentRequestBody);
		
		ResponseEntity<byte[]> resEntityByte = null;
		//File resEntityByte = null;
		resEntityByte = baseBiz.viewDoc(documentRequestBody);
		
		HttpHeaders resHeaders = resEntityByte.getHeaders();
		resHeaders.getContentDisposition();
		ContentDisposition disposition = resHeaders.getContentDisposition();
		String filename = disposition.getFilename();
		//String fileName = disposition.replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
		//MediaType contentType = resEntityByte.getHeaders().getContentType();
		MediaType contentType = getMediaTypeForFileName(this.servletContext,filename);
		response.setHeader("content-disposition", "inline;filename=" + URLEncoder.encode(filename, "UTF-8"));
		response.setContentType(contentType.toString());
		//FileInputStream in = new FileInputStream(resEntityByte);
		OutputStream out = response.getOutputStream();
//		byte buffer[] = new byte[1024];
//		int len = 0;
//		while ((len = in.read(buffer)) > 0) {
//			out.write(buffer, 0, len);
//		}
//		in.close();
		out.write(resEntityByte.getBody());
		out.close();
		

	}
	
    public MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
        // application/pdf
        // application/xml
        // image/gif, ...
        String mineType = servletContext.getMimeType(fileName);
        try {
            MediaType mediaType = MediaType.parseMediaType(mineType);
            return mediaType;
        } catch (Exception e) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
	

    
}