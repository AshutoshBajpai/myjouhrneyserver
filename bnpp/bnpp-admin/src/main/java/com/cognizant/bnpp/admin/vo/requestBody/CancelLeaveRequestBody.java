package com.cognizant.bnpp.admin.vo.requestBody;

public class CancelLeaveRequestBody {
	private String employeeUid;
	private int empLeaveAppId;
	private String reason;
	private String action;

	public String getEmployeeUid() {
		return employeeUid;
	}

	public void setEmployeeUid(String employeeUid) {
		this.employeeUid = employeeUid;
	}

	public int getEmpLeaveAppId() {
		return empLeaveAppId;
	}

	public void setEmpLeaveAppId(int empLeaveAppId) {
		this.empLeaveAppId = empLeaveAppId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
