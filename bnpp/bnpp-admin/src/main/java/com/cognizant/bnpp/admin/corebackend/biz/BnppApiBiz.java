package com.cognizant.bnpp.admin.corebackend.biz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.bnpp.admin.corebackend.feign.BnppAPIFeign;
import com.cognizant.bnpp.admin.corebackend.feign.UploadFileFeign;
import com.cognizant.bnpp.admin.vo.CompLeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.CompPendingRequestVo;
import com.cognizant.bnpp.admin.vo.LeaveAppType;
import com.cognizant.bnpp.admin.vo.LeaveApplicationResponse;
import com.cognizant.bnpp.admin.vo.LeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.PendingRequestVo;
import com.cognizant.bnpp.admin.vo.TeamCalendarVo;
import com.cognizant.bnpp.admin.vo.requestBody.ApproveOrRejectRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.CancelLeaveRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.ChangeApproverRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.DocumentRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.HolidayWorkRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.MarkBLRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.TeamCalendarRequestBody;
import com.cognizant.bnpp.common.msg.BaseResponse;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import feign.FeignException;

@Service
public class BnppApiBiz {
	@Autowired
	BnppAPIFeign bnppAPIFeign;
	@Autowired
	UploadFileFeign uploadFileFeign;
	@Autowired
	Environment env;


	public ObjectRestResponse<String> authorize(String empno) throws Exception {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.authorize(empno);
		} catch (FeignException fe) {
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e:" + e.toString());
		}
		return response;
	}

	public ObjectRestResponse<?> getLeaveBalance(String empno) {
		ObjectRestResponse<?> response = null;
		try {
			response = bnppAPIFeign.getLeaveBalance(empno);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<?> getCompLeaveBalance(String empno) {
		ObjectRestResponse<?> response = null;
		try {
			response = bnppAPIFeign.getCompLeaveBalance(empno);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<List<TeamCalendarVo>> getTeamCalendar(TeamCalendarRequestBody teamCalendarRequestBody)
			throws Exception {
		ObjectRestResponse<List<TeamCalendarVo>> response = new ObjectRestResponse<List<TeamCalendarVo>>();
		try {
			response = bnppAPIFeign.getTeamCalendar(teamCalendarRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<List<LeaveHistoryItemVo>> getLeaveHistory(
			LeaveHistoryRequestBody leaveHistoryRequestBody) {
		ObjectRestResponse<List<LeaveHistoryItemVo>> response = new ObjectRestResponse<List<LeaveHistoryItemVo>>();
		try {
			response = bnppAPIFeign.getLeaveHistory(leaveHistoryRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}

		return response;
	}

	public ObjectRestResponse<List<CompLeaveHistoryItemVo>> getCompLeaveHistory(
			LeaveHistoryRequestBody leaveHistoryRequestBody) {
		ObjectRestResponse<List<CompLeaveHistoryItemVo>> response = new ObjectRestResponse<List<CompLeaveHistoryItemVo>>();
		try {
			response = bnppAPIFeign.getCompLeaveHistory(leaveHistoryRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}

		return response;
	}

	public ObjectRestResponse<String> cancelLeave(CancelLeaveRequestBody cancelLeaveRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.cancelLeave(cancelLeaveRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<String> changeApprover(ChangeApproverRequestBody changeApproverRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.changeApprover(changeApproverRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<String> markBL(@RequestBody MarkBLRequestBody markBLRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.markBL(markBLRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<Integer> getPendingNumber(String empno) {
		ObjectRestResponse<Integer> response = new ObjectRestResponse<Integer>();
		// String res = null;
		try {
			response = bnppAPIFeign.getPendingNumber(empno);
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// JSONObject resJSONObj = JSONObject.parseObject(res);
		// response.setMessage(resJSONObj.getString("pendingNumber"));
		return response;
	}

	public ObjectRestResponse<List<PendingRequestVo>> getPendingRequest(String empno) {
		ObjectRestResponse<List<PendingRequestVo>> response = new ObjectRestResponse<List<PendingRequestVo>>();
		try {
			response = bnppAPIFeign.getPendingRequest(empno);
		} catch (FeignException fe) {
			// TODO Auto-generated catch block
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<List<CompPendingRequestVo>> getCompPendingRequest(String empno) {
		ObjectRestResponse<List<CompPendingRequestVo>> response = new ObjectRestResponse<List<CompPendingRequestVo>>();
		List<CompPendingRequestVo> compPendingRequestList = new ArrayList<CompPendingRequestVo>();
		try {
			response = bnppAPIFeign.getCompPendingRequest(empno);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (response.getData() == null) {
			response.setData(compPendingRequestList);
		}
		return response;
	}

	public ObjectRestResponse<String> approve(ApproveOrRejectRequestBody approveOrRejectRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.approve(approveOrRejectRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<String> reject(ApproveOrRejectRequestBody approveOrRejectRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.reject(approveOrRejectRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;

	}

	public ObjectRestResponse<List<LeaveAppType>> getLeaveAppSetup(@PathVariable(value = "empno") String empno) {
		ObjectRestResponse<List<LeaveAppType>> response = new ObjectRestResponse<List<LeaveAppType>>();
		try {
			response = bnppAPIFeign.getLeaveAppSetup(empno);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;

	}

	public ObjectRestResponse<String> holidayWorkRequest(HolidayWorkRequestBody holidayWorkRequestBody) {
		ObjectRestResponse<String> response = new ObjectRestResponse<String>();
		try {
			response = bnppAPIFeign.holidayWorkRequest(holidayWorkRequestBody);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ObjectRestResponse<?> leaveRequest(String param,MultipartFile srcFile) {
		ObjectRestResponse response = new ObjectRestResponse();
		
		LeaveApplicationResponse msg = new LeaveApplicationResponse();
		ResponseEntity<String> response1 = null;
		String tempFileName = "";
		try {
			
		      MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		      bodyMap.add("request", param);
		      
		      if(srcFile!=null) {
		    	  FileOutputStream fo;
			      //bodyMap.add("file", new ByteArrayResource(srcFile.getBytes()));
		    	 
		    	 try {
		    		 if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") == 0) {
		    			 tempFileName= "C:\\Users\\485693\\BNPP_Chattbot_Docs\\app_server_releases\\App_Proxy_Temp\\" + srcFile.getOriginalFilename();
		    		    }
		    		    else {
		    		        
		    		    	tempFileName= "/tmp/" + srcFile.getOriginalFilename();
		    		    }
		    	   
		    	  fo = new FileOutputStream(tempFileName);
		            fo.write(srcFile.getBytes());
		            fo.close();
		    	 }catch(Exception e) {
		    		 
		    	 }
			      bodyMap.add("file", new FileSystemResource(tempFileName));
			      System.out.println(bodyMap);
				  //bodyMap.add("file", srcFile);
		      
		      }
		      HttpHeaders headers = new HttpHeaders();
		      headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		      HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(bodyMap, headers);
 
		      
		      String path = env.getProperty("corebackendurl");
		     
		      RestTemplate restTemplate = new RestTemplate();
		      response1 = restTemplate.exchange(path+"/leaveApplication",
		              HttpMethod.POST, requestEntity, String.class);
		      
		    /* String  response2 = restTemplate.postForObject(path+"/leaveApplication", requestEntity, String.class);*/
		      System.out.println("response status: " + response1.getStatusCode());
		      System.out.println("response body: " + response1.getBody());
		  
		      File file = new File(tempFileName); 
	          
		        if(file.delete()) 
		        { 
		            System.out.println("File deleted successfully"); 
		        } 
		        
		     // response.setData(response1.getBody());
		     // response.setStatus(response1.getStatusCodeValue());
		       
		        response = new ObjectMapper().readValue(response1.getBody(), ObjectRestResponse.class);
		        
		     //   if(!response.getData().isEmpty() ) {
			  //      if(!isJSONValid(response.getData().toString())) {
			        	if(!response.getData().toString().contains("message=")) {
			        	
			        	//messageResponse = "message"+":"+;
			        	msg.setMessage(response.getData().toString());
			        	//JSONObject jsonObject = new JSONObject();
			        	//jsonObject.put("message", response.getData());
			        	//response.setData(new ObjectMapper().readValue(messageResponse, String.class));
			        	response.setData(msg);
			 
			        }
		        //}
		       
		      System.out.println("response Data: " + response.getData());
		      System.out.println("response Status: " + response.getStatus());
		}  catch (Exception e) {
			// TODO: handle exception
		      File file = new File(tempFileName); 
		        if(file.delete()) 
		        { 
		            System.out.println("File deleted successfully"); 
		        }
		}
		return response;
		
	}
	
	public ObjectRestResponse<?> leaveRequestWithoutDoc(String param) {
		ObjectRestResponse<?> response = null;
		try {
			//response = uploadFileFeign.handleFileUploadWithoutDoc(param);
			ObjectMapper mapper = new ObjectMapper();
			LeaveRequestBody obj = mapper.readValue(param, LeaveRequestBody.class);
			//response = uploadFileFeign.handleFileUploadWithoutDoc(param);
			response = bnppAPIFeign.leaveRequest(obj);
		} catch (FeignException fe) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public ResponseEntity<byte[]> viewDoc(DocumentRequestBody documentRequestBody) {
		ResponseEntity<byte[]> entity = null;
		//HttpServletResponse entity = null;
		try {
			entity = bnppAPIFeign.viewDoc(documentRequestBody);
			//entity = bnppAPIFeign.viewDoc(documentRequestBody);
			
		} catch (FeignException fe) {
			// TODO: handle exception
			System.out.println("fe:" + fe.toString());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e:" + e.toString());
		}
		return entity;
	}
	
}
