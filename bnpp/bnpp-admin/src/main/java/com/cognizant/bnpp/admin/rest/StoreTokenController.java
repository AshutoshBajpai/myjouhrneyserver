package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppStaffBiz;
import com.cognizant.bnpp.admin.entity.BnppStaff;
import com.cognizant.bnpp.admin.vo.ChatbotInfo;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/storeToken")
public class StoreTokenController extends BaseController<BnppStaffBiz, BnppStaff> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<String> storeMobileToken(@RequestParam("empno") String empno, @RequestParam("token") String token,
			HttpServletRequest request
			) {
		
		ObjectRestResponse<String> abc = new ObjectRestResponse<String>();
		
		HttpSession session = request.getSession();
		HashMap<String, String> hmpAttribute  =null;
		if(session.getAttribute("NotificationToken") == null) {
			/*HashMap<String, ArrayList<String>> hmpAttribute = new HashMap<String, ArrayList<String>>();
			ArrayList<String>  arr = new ArrayList<String>();*/
			 hmpAttribute = new HashMap<String, String>();
			
			hmpAttribute.put(empno, token);
			
			session.setAttribute("NotificationToken", hmpAttribute);
			abc.setData(session.getAttribute("NotificationToken").toString());
		}
		
		else {
			hmpAttribute = (HashMap<String, String>) session.getAttribute("NotificationToken");
			hmpAttribute.put(empno, token);
			session.setAttribute("NotificationToken", hmpAttribute);
			abc.setData(session.getAttribute("NotificationToken").toString());
		}
//		session.setAttribute(name, value);
		
		return abc;
		
		
	}

}