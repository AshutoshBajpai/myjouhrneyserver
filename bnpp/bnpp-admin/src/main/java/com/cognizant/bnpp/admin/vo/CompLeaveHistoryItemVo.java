package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class CompLeaveHistoryItemVo {
	private int allowanceID;
	private float daysApplied;
	private String reason;
	private String applStatus;
	private Date holidayDate;
	private Date validFrom;
	private Date validTill;
	private String extensionAllowed;
	private String cancellationAllowed;
	private String changeApproverAllowed;
	private float daysConsumed;
	private String primaryApprovalName;
	private String backupApprovalName;

	public int getAllowanceID() {
		return allowanceID;
	}

	public void setAllowanceID(int allowanceID) {
		this.allowanceID = allowanceID;
	}

	public float getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(float daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getApplStatus() {
		return applStatus;
	}

	public void setApplStatus(String applStatus) {
		this.applStatus = applStatus;
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public String getExtensionAllowed() {
		return extensionAllowed;
	}

	public void setExtensionAllowed(String extensionAllowed) {
		this.extensionAllowed = extensionAllowed;
	}

	public String getCancellationAllowed() {
		return cancellationAllowed;
	}

	public void setCancellationAllowed(String cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	public String getChangeApproverAllowed() {
		return changeApproverAllowed;
	}

	public void setChangeApproverAllowed(String changeApproverAllowed) {
		this.changeApproverAllowed = changeApproverAllowed;
	}

	public float getDaysConsumed() {
		return daysConsumed;
	}

	public void setDaysConsumed(float daysConsumed) {
		this.daysConsumed = daysConsumed;
	}

	public String getPrimaryApprovalName() {
		return primaryApprovalName;
	}

	public void setPrimaryApprovalName(String primaryApprovalName) {
		this.primaryApprovalName = primaryApprovalName;
	}

	public String getBackupApprovalName() {
		return backupApprovalName;
	}

	public void setBackupApprovalName(String backupApprovalName) {
		this.backupApprovalName = backupApprovalName;
	}

	public CompLeaveHistoryItemVo() {
		super();
	}

	public CompLeaveHistoryItemVo(int allowanceID, float daysApplied, String reason, String applStatus,
			Date holidayDate, Date validFrom, Date validTill, String extensionAllowed, String cancellationAllowed,
			String changeApproverAllowed, float daysConsumed, String primaryApprovalName, String backupApprovalName) {
		super();
		this.allowanceID = allowanceID;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.applStatus = applStatus;
		this.holidayDate = holidayDate;
		this.validFrom = validFrom;
		this.validTill = validTill;
		this.extensionAllowed = extensionAllowed;
		this.cancellationAllowed = cancellationAllowed;
		this.changeApproverAllowed = changeApproverAllowed;
		this.daysConsumed = daysConsumed;
		this.primaryApprovalName = primaryApprovalName;
		this.backupApprovalName = backupApprovalName;
	}

}
