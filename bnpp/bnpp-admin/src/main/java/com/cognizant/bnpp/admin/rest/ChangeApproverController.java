package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppLeaveBiz;
import com.cognizant.bnpp.admin.entity.BnppLeave;
import com.cognizant.bnpp.admin.vo.requestBody.ChangeApproverRequestBody;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/changeApprover")
public class ChangeApproverController extends BaseController<BnppLeaveBiz, BnppLeave> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<String> approve(@RequestBody ChangeApproverRequestBody changeApproverRequestBody)
			throws Exception {
		// TODO
		return baseBiz.changeApprover(changeApproverRequestBody);
	}
}