package com.cognizant.bnpp.admin.vo.requestBody;

public class DocumentRequestBody {
	private String empNo;
	private int empLeaveApplicationId;

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public int getEmpLeaveApplicationId() {
		return empLeaveApplicationId;
	}

	public void setEmpLeaveApplicationId(int empLeaveApplicationId) {
		this.empLeaveApplicationId = empLeaveApplicationId;
	}

}
