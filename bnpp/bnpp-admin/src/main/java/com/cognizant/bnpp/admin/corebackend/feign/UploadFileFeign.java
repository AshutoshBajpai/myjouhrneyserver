package com.cognizant.bnpp.admin.corebackend.feign;

import java.util.Map;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.bnpp.common.msg.ObjectRestResponse;

import feign.Headers;
import feign.Param;
import feign.codec.Encoder;
import feign.form.FormEncoder;
import feign.form.spring.SpringFormEncoder;

@FeignClient(url = "${corebackendurl}", name = "uploadfile", configuration = UploadFileFeign.MultipartSupportConfig.class)
//@FeignClient(url = "${corebackendurl}", name = "uploadfile")
public interface UploadFileFeign {
	// upload doc
	/*
	 * @PostMapping(value = "uploadDoc") public ObjectRestResponse<String>
	 * uploadDoc(HttpServletRequest request,
	 * 
	 * @RequestParam(value = "param") String param);
	 */
/*	@PostMapping(value = "leaveApplication", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@Headers("Content-Type: multipart/form-data")
	public ObjectRestResponse<?> handleFileUpload(@RequestParam(value = "request") String request,
			@RequestPart(value = "file") MultipartFile srcFile);*/
	
	   @RequestMapping(method = RequestMethod.POST,value="leaveApplication")
		   @Headers("Content-Type: multipart/form-data")
	   /*public ObjectRestResponse<?> handleFileUpload(@RequestParam(value = "request") String request,
				@RequestPart(value = "file") MultipartFile srcFile);*/  
	   public ObjectRestResponse<?> handleFileUpload(@RequestBody Map<String, ?> formParams);

	   
	   
	@PostMapping(value = "leaveApplication")
	public ObjectRestResponse<?> handleFileUploadWithoutDoc(@RequestPart(value = "request") String request);
	
	class MultipartSupportConfig {
/*		@Autowired
		private ObjectFactory<HttpMessageConverters> messageConverters;*/
		
		@Bean
		public Encoder feignFormEncoder() {
		//	return new FeignSpringFormEncoder();
		//	 return new FormEncoder(new SpringEncoder(this.messageConverters));
		//	 return new FormEncoder();
			 return new SpringFormEncoder();
		}

		@Bean
		public feign.Logger.Level multipartLoggerLevel() {
			return feign.Logger.Level.FULL;
		}
	}	

}
