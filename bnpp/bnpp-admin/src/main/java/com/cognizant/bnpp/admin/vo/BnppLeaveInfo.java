package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class BnppLeaveInfo {
	private int empLeaveApplicationID;
	private Date dateFrom;
	private Date dateTo;
	private String leaveOptionFrom;
	private String leaveOptionTo;
	private String reason;
	private String leaveType;
	private String empNo;
	private String action;
	private int leaveCategory;
	private int daysApplied;
	private String leaveStatus;
	private String cancellationAllowed;
	private String cancelReasonInd;
	private String blockLeaveEligible;
	private String documentUploadOptionInd;
	private String uploadDocumentName;
	private String documentViewOptionInd;
	private String rejectReasonInd;

	public int getEmpLeaveApplicationID() {
		return empLeaveApplicationID;
	}

	public void setEmpLeaveApplicationID(int empLeaveApplicationID) {
		this.empLeaveApplicationID = empLeaveApplicationID;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getLeaveOptionFrom() {
		return leaveOptionFrom;
	}

	public void setLeaveOptionFrom(String leaveOptionFrom) {
		this.leaveOptionFrom = leaveOptionFrom;
	}

	public String getLeaveOptionTo() {
		return leaveOptionTo;
	}

	public void setLeaveOptionTo(String leaveOptionTo) {
		this.leaveOptionTo = leaveOptionTo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getLeaveCategory() {
		return leaveCategory;
	}

	public void setLeaveCategory(int leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	public int getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(int daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getCancellationAllowed() {
		return cancellationAllowed;
	}

	public void setCancellationAllowed(String cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	public String getCancelReasonInd() {
		return cancelReasonInd;
	}

	public void setCancelReasonInd(String cancelReasonInd) {
		this.cancelReasonInd = cancelReasonInd;
	}

	public String getBlockLeaveEligible() {
		return blockLeaveEligible;
	}

	public void setBlockLeaveEligible(String blockLeaveEligible) {
		this.blockLeaveEligible = blockLeaveEligible;
	}

	public String getDocumentUploadOptionInd() {
		return documentUploadOptionInd;
	}

	public void setDocumentUploadOptionInd(String documentUploadOptionInd) {
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

	public String getUploadDocumentName() {
		return uploadDocumentName;
	}

	public void setUploadDocumentName(String uploadDocumentName) {
		this.uploadDocumentName = uploadDocumentName;
	}

	public String getDocumentViewOptionInd() {
		return documentViewOptionInd;
	}

	public void setDocumentViewOptionInd(String documentViewOptionInd) {
		this.documentViewOptionInd = documentViewOptionInd;
	}

	public String getRejectReasonInd() {
		return rejectReasonInd;
	}

	public void setRejectReasonInd(String rejectReasonInd) {
		this.rejectReasonInd = rejectReasonInd;
	}

	public BnppLeaveInfo(int empLeaveApplicationID, Date dateFrom, Date dateTo, String leaveOptionFrom,
			String leaveOptionTo, String reason, String leaveType, String empNo, String action, int leaveCategory,
			int daysApplied, String leaveStatus, String cancellationAllowed, String cancelReasonInd,
			String blockLeaveEligible, String documentUploadOptionInd, String uploadDocumentName,
			String documentViewOptionInd, String rejectReasonInd) {
		super();
		this.empLeaveApplicationID = empLeaveApplicationID;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.leaveOptionFrom = leaveOptionFrom;
		this.leaveOptionTo = leaveOptionTo;
		this.reason = reason;
		this.leaveType = leaveType;
		this.empNo = empNo;
		this.action = action;
		this.leaveCategory = leaveCategory;
		this.daysApplied = daysApplied;
		this.leaveStatus = leaveStatus;
		this.cancellationAllowed = cancellationAllowed;
		this.cancelReasonInd = cancelReasonInd;
		this.blockLeaveEligible = blockLeaveEligible;
		this.documentUploadOptionInd = documentUploadOptionInd;
		this.uploadDocumentName = uploadDocumentName;
		this.documentViewOptionInd = documentViewOptionInd;
		this.rejectReasonInd = rejectReasonInd;
	}

}
