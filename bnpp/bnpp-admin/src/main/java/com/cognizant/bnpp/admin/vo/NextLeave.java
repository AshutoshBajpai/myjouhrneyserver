package com.cognizant.bnpp.admin.vo;

import java.io.Serializable;
import java.util.Date;

public class NextLeave implements Serializable {
	private static final long serialVersionUID = 1L;
	private int empLeaveApplicationId;
	private Date dateFrom;
	private Date dateTo;
	private int daysApplied;
	private String reason;
	private String leaveType;
	private String leaveStatus;
	private String blockLeaveTagged;

	public NextLeave() {
		super();
	}

	public int getEmpLeaveApplicationId() {
		return empLeaveApplicationId;
	}

	public void setEmpLeaveApplicationId(int empLeaveApplicationId) {
		this.empLeaveApplicationId = empLeaveApplicationId;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public int getDaysApplied() {
		return daysApplied;
	}

	public void setDaysApplied(int daysApplied) {
		this.daysApplied = daysApplied;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getBlockLeaveTagged() {
		return blockLeaveTagged;
	}

	public void setBlockLeaveTagged(String blockLeaveTagged) {
		this.blockLeaveTagged = blockLeaveTagged;
	}

	public NextLeave(int empLeaveApplicationId, Date dateFrom, Date dateTo, int daysApplied, String reason,
			String leaveType, String leaveStatus, String blockLeaveTagged) {
		super();
		this.empLeaveApplicationId = empLeaveApplicationId;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.daysApplied = daysApplied;
		this.reason = reason;
		this.leaveType = leaveType;
		this.leaveStatus = leaveStatus;
		this.blockLeaveTagged = blockLeaveTagged;
	}

}
