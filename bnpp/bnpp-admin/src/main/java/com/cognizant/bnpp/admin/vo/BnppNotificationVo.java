package com.cognizant.bnpp.admin.vo;

import java.util.Date;

public class BnppNotificationVo {
	private int notificationId;
	private String submitMsg;
	private String approveMsg;
	private Date date;

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubmitMsg() {
		return submitMsg;
	}

	public void setSubmitMsg(String submitMsg) {
		this.submitMsg = submitMsg;
	}

	public String getApproveMsg() {
		return approveMsg;
	}

	public void setApproveMsg(String approveMsg) {
		this.approveMsg = approveMsg;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
