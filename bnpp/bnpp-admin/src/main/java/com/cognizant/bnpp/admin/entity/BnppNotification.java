package com.cognizant.bnpp.admin.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 推送消息员工联合表
 * 
 * @author Likui.Mao
 * @email Likui.Mao@cognizant.com
 * @date 2018-10-18 16:42:21
 */
public class BnppNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	// 推送消息编号
	private Integer notificationId;

	// 员工身份编号
	private String empNo;

	// 提交信息
	private String submitMsg;

	// 批准信息
	private String approveMsg;

	// 创建时间
	private Date createDate;

	/**
	 * 设置：推送消息编号
	 */
	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}

	/**
	 * 获取：推送消息编号
	 */
	public Integer getNotificationId() {
		return notificationId;
	}

	/**
	 * 设置：员工身份编号
	 */
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	/**
	 * 获取：员工身份编号
	 */
	public String getEmpNo() {
		return empNo;
	}

	/**
	 * 设置：提交信息
	 */
	public void setSubmitMsg(String submitMsg) {
		this.submitMsg = submitMsg;
	}

	/**
	 * 获取：提交信息
	 */
	public String getSubmitMsg() {
		return submitMsg;
	}

	/**
	 * 设置：批准信息
	 */
	public void setApproveMsg(String approveMsg) {
		this.approveMsg = approveMsg;
	}

	/**
	 * 获取：批准信息
	 */
	public String getApproveMsg() {
		return approveMsg;
	}

	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}

}
