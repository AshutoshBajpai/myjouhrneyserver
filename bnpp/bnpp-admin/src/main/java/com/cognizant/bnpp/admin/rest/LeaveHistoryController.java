package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppStaffBiz;
import com.cognizant.bnpp.admin.entity.BnppStaff;
import com.cognizant.bnpp.admin.vo.LeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/leaveHistory")
public class LeaveHistoryController extends BaseController<BnppStaffBiz, BnppStaff> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<List<LeaveHistoryItemVo>> getLeaveHistory(
			@RequestBody LeaveHistoryRequestBody leaveHistoryRequestBody) throws Exception {
		// TODO
		leaveHistoryRequestBody.setType("valid");
		// get leaveList
		return baseBiz.getLeaveHistory(leaveHistoryRequestBody);
	}
}