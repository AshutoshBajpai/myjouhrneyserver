package com.cognizant.bnpp.admin.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.bnpp.admin.corebackend.biz.BnppApiBiz;
import com.cognizant.bnpp.admin.vo.CompLeaveHistoryItemVo;
import com.cognizant.bnpp.admin.vo.requestBody.HolidayWorkRequestBody;
import com.cognizant.bnpp.admin.vo.requestBody.LeaveHistoryRequestBody;
import com.cognizant.bnpp.common.biz.BaseBiz;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;

/**
 * 加班申请及员工联合表
 *
 * @author Likui.Mao
 * @email Likui.Mao@cognizant.com
 * @date 2018-10-10 18:57:27
 */
@Service
public class BnppHolidayWorkBiz extends BaseBiz {
	@Autowired
	private BnppApiBiz bnppApiBiz;

	public ObjectRestResponse<List<CompLeaveHistoryItemVo>> getCompLeaveHistoryList(
			LeaveHistoryRequestBody leaveHistoryRequestBody) {
		return bnppApiBiz.getCompLeaveHistory(leaveHistoryRequestBody);
	}

	public ObjectRestResponse<String> insertHolidayWork(HolidayWorkRequestBody holidayWorkRequestBody)
			throws Exception {
		return bnppApiBiz.holidayWorkRequest(holidayWorkRequestBody);
	}
}