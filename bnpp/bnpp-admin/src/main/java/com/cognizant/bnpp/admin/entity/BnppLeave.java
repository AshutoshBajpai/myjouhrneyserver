package com.cognizant.bnpp.admin.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 假期员工联合表
 * 
 * @author Likui.Mao
 * @email Likui.Mao@cognizant.com
 * @date 2018-09-26 15:25:00
 */
public class BnppLeave implements Serializable {
	private static final long serialVersionUID = 1L;
	// 假日应用编号
	private Integer empLeaveApplicationId;

	// 起始日期
	private Date dateFrom;

	// 结束日期
	private Date dateTo;

	// 假日选项开始时刻
	private String leaveOptionFrom;

	// 假日选项结束时刻
	private String leaveOptionTo;

	// 原因
	private String reason;

	// 请假类型
	private String leaveType;

	// 员工身份编号
	private String empNo;

	// 用户行为
	private String action;

	// 请假类别(1: Normal Pending Leaves,2:HR Pending Leaves,3:Comp Off Pending
	// Requests)
	private Integer leaveCategory;

	// 申请日
	private Integer daysApplied;

	// 请假状态
	private String leaveStatus;

	// 是否取消标记
	private String cancellationAllowed;

	// 取消原因标记
	private String cancelReasonInd;

	// 允许标记为强制请假标记
	private String blockLeaveEligible;

	// 文档上传选项标记
	private String documentUploadOptionInd;

	// 文档名字类型
	private String documentUploadFileNamePattern;

	// 上传文档名字
	private String uploadDocumentName;

	// 附件文档标记
	private String documentViewOptionInd;

	// 拒绝原因标记
	private String rejectReasonInd;

	// 强制休假标记
	private String blockLeaveTagged;

	//
	private String primaryApprovalName;

	//
	private String backupApprovalName;

	private String rejectReason;

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getPrimaryApprovalName() {
		return primaryApprovalName;
	}

	public void setPrimaryApprovalName(String primaryApprovalName) {
		this.primaryApprovalName = primaryApprovalName;
	}

	public String getBackupApprovalName() {
		return backupApprovalName;
	}

	public void setBackupApprovalName(String backupApprovalName) {
		this.backupApprovalName = backupApprovalName;
	}

	public String getBlockLeaveTagged() {
		return blockLeaveTagged;
	}

	public void setBlockLeaveTagged(String blockLeaveTagged) {
		this.blockLeaveTagged = blockLeaveTagged;
	}

	/**
	 * 设置：假日应用编号
	 */
	public void setEmpLeaveApplicationId(Integer empLeaveApplicationId) {
		this.empLeaveApplicationId = empLeaveApplicationId;
	}

	/**
	 * 获取：假日应用编号
	 */
	public Integer getEmpLeaveApplicationId() {
		return empLeaveApplicationId;
	}

	/**
	 * 设置：起始日期
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * 获取：起始日期
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * 设置：结束日期
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * 获取：结束日期
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * 设置：假日选项开始时刻
	 */
	public void setLeaveOptionFrom(String leaveOptionFrom) {
		this.leaveOptionFrom = leaveOptionFrom;
	}

	/**
	 * 获取：假日选项开始时刻
	 */
	public String getLeaveOptionFrom() {
		return leaveOptionFrom;
	}

	/**
	 * 设置：假日选项结束时刻
	 */
	public void setLeaveOptionTo(String leaveOptionTo) {
		this.leaveOptionTo = leaveOptionTo;
	}

	/**
	 * 获取：假日选项结束时刻
	 */
	public String getLeaveOptionTo() {
		return leaveOptionTo;
	}

	/**
	 * 设置：原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 获取：原因
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * 设置：请假类型
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	/**
	 * 获取：请假类型
	 */
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * 设置：员工身份编号
	 */
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	/**
	 * 获取：员工身份编号
	 */
	public String getEmpNo() {
		return empNo;
	}

	/**
	 * 设置：用户行为
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * 获取：用户行为
	 */
	public String getAction() {
		return action;
	}

	/**
	 * 设置：请假类别(1: Normal Pending Leaves,2:HR Pending Leaves,3:Comp Off Pending
	 * Requests)
	 */
	public void setLeaveCategory(Integer leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	/**
	 * 获取：请假类别(1: Normal Pending Leaves,2:HR Pending Leaves,3:Comp Off Pending
	 * Requests)
	 */
	public Integer getLeaveCategory() {
		return leaveCategory;
	}

	/**
	 * 设置：申请日
	 */
	public void setDaysApplied(Integer daysApplied) {
		this.daysApplied = daysApplied;
	}

	/**
	 * 获取：申请日
	 */
	public Integer getDaysApplied() {
		return daysApplied;
	}

	/**
	 * 设置：请假状态
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	/**
	 * 获取：请假状态
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}

	/**
	 * 设置：是否取消标记
	 */
	public void setCancellationAllowed(String cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	/**
	 * 获取：是否取消标记
	 */
	public String getCancellationAllowed() {
		return cancellationAllowed;
	}

	/**
	 * 设置：取消原因标记
	 */
	public void setCancelReasonInd(String cancelReasonInd) {
		this.cancelReasonInd = cancelReasonInd;
	}

	/**
	 * 获取：取消原因标记
	 */
	public String getCancelReasonInd() {
		return cancelReasonInd;
	}

	/**
	 * 设置：允许标记为强制请假标记
	 */
	public void setBlockLeaveEligible(String blockLeaveEligible) {
		this.blockLeaveEligible = blockLeaveEligible;
	}

	/**
	 * 获取：允许标记为强制请假标记
	 */
	public String getBlockLeaveEligible() {
		return blockLeaveEligible;
	}

	/**
	 * 设置：文档上传选项标记
	 */
	public void setDocumentUploadOptionInd(String documentUploadOptionInd) {
		this.documentUploadOptionInd = documentUploadOptionInd;
	}

	/**
	 * 获取：文档上传选项标记
	 */
	public String getDocumentUploadOptionInd() {
		return documentUploadOptionInd;
	}

	/**
	 * 设置：文档名字类型
	 */
	public void setDocumentUploadFileNamePattern(String documentUploadFileNamePattern) {
		this.documentUploadFileNamePattern = documentUploadFileNamePattern;
	}

	/**
	 * 获取：文档名字类型
	 */
	public String getDocumentUploadFileNamePattern() {
		return documentUploadFileNamePattern;
	}

	/**
	 * 设置：上传文档名字
	 */
	public void setUploadDocumentName(String uploadDocumentName) {
		this.uploadDocumentName = uploadDocumentName;
	}

	/**
	 * 获取：上传文档名字
	 */
	public String getUploadDocumentName() {
		return uploadDocumentName;
	}

	/**
	 * 设置：附件文档标记
	 */
	public void setDocumentViewOptionInd(String documentViewOptionInd) {
		this.documentViewOptionInd = documentViewOptionInd;
	}

	/**
	 * 获取：附件文档标记
	 */
	public String getDocumentViewOptionInd() {
		return documentViewOptionInd;
	}

	/**
	 * 设置：拒绝原因标记
	 */
	public void setRejectReasonInd(String rejectReasonInd) {
		this.rejectReasonInd = rejectReasonInd;
	}

	/**
	 * 获取：拒绝原因标记
	 */
	public String getRejectReasonInd() {
		return rejectReasonInd;
	}
}
