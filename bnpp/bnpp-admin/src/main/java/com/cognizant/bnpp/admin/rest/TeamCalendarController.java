package com.cognizant.bnpp.admin.rest;

import com.cognizant.bnpp.admin.biz.BnppLeaveBiz;
import com.cognizant.bnpp.admin.entity.BnppLeave;
import com.cognizant.bnpp.admin.vo.TeamCalendarVo;
import com.cognizant.bnpp.admin.vo.requestBody.TeamCalendarRequestBody;
import com.cognizant.bnpp.common.msg.ObjectRestResponse;
import com.cognizant.bnpp.common.rest.BaseController;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("las/rest/services/teamCalendar")
public class TeamCalendarController extends BaseController<BnppLeaveBiz, BnppLeave> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<List<TeamCalendarVo>> getTeamCalendar(
			@RequestBody TeamCalendarRequestBody teamCalendarRequestBody) throws Exception {
		// TODO
		return baseBiz.getTeamCalendar(teamCalendarRequestBody);
	}
}