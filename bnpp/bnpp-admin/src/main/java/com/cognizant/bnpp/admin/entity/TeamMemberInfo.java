package com.cognizant.bnpp.admin.entity;

public class TeamMemberInfo {
	private String empSapName;
	private String leaveType;
	private String leaveOption;

	public String getEmpSapName() {
		return empSapName;
	}

	public void setEmpSapName(String empSapName) {
		this.empSapName = empSapName;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveOption() {
		return leaveOption;
	}

	public void setLeaveOption(String leaveOption) {
		this.leaveOption = leaveOption;
	}

	public TeamMemberInfo() {
		super();
	}

	public TeamMemberInfo(String empSapName, String leaveType, String leaveOption) {
		super();
		this.empSapName = empSapName;
		this.leaveType = leaveType;
		this.leaveOption = leaveOption;
	}

}
