package com.cognizant.bnpp.common.exception;

import com.cognizant.bnpp.common.constant.CommonConstants;

public class InsertHolidayWorkException extends BaseException{

	public InsertHolidayWorkException(String message) {
		super(message, CommonConstants.INSERT_HOLIDAY_WORK_ERROR_CODE);
	}
}
