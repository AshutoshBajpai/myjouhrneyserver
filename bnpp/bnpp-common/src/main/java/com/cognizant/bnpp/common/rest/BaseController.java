package com.cognizant.bnpp.common.rest;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.bnpp.common.biz.BaseBiz;

import javax.servlet.http.HttpServletRequest;

/**
 * ${DESCRIPTION}
 *
 * @author ctsdeveloper
 * @create 2017-06-15 8:48
 */
public class BaseController<Biz extends BaseBiz, Entity> {
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected Biz baseBiz;
}
