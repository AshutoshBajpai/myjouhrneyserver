package com.cognizant.bnpp.common.exception;

import com.cognizant.bnpp.common.constant.CommonConstants;

public class InsertLeaveException extends BaseException{

	public InsertLeaveException(String message) {
		super(message, CommonConstants.INSERT_LEAVE_ERROR_CODE);
	}
}
