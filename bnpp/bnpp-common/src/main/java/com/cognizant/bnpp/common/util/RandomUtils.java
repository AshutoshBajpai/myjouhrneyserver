package com.cognizant.bnpp.common.util;

import java.io.IOException;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RandomUtils {

	public static int generateRandom(int min, int max) {
		Random random = new Random();
		return random.nextInt(max) % (max - min + 1) + min;
	}
	

	  public static boolean isJSONValid(String jsonInString ) {
		    try {
		       final ObjectMapper mapper = new ObjectMapper();
		       mapper.readTree(jsonInString);
		       return true;
		    } catch (IOException e) {
		       return false;
		    }
		  }

}
