package com.cognizant.bnpp.common.exception;

import com.cognizant.bnpp.common.constant.CommonConstants;

/**
 * 
 * @author 686347 ziqi
 * @version 2018/7/25
 * @email Ziqi.Liu@cognizant.com
 *
 */

public class LeaveDayExcessException extends BaseException{
	
	private static final long serialVersionUID = 1485357482746829396L;

	public LeaveDayExcessException(String message) {
		super(message, CommonConstants.LEAVE_DAYS_EXCESS_ERROR_CODE);
	}
}
