package com.cognizant.bnpp.common.msg;

/**
 * Created by ctsdeveloper on 2017/6/11.
 */
public class ObjectRestResponse<T> extends BaseResponse {

	T data;

	public ObjectRestResponse data(T data) {
		this.setData(data);
		return this;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
