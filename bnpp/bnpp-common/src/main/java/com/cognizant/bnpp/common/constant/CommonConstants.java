package com.cognizant.bnpp.common.constant;

/**
 * Created by ctsdeveloper on 2017/8/29.
 */
public class CommonConstants {
	public final static String RESOURCE_TYPE_MENU = "menu";
	public final static String RESOURCE_TYPE_BTN = "button";
	public static final Integer EX_TOKEN_ERROR_CODE = 40101;
	// 用户token异常
	public static final Integer EX_USER_INVALID_CODE = 40001;
	// 客户端token异常
	public static final Integer EX_CLIENT_INVALID_CODE = 40131;
	public static final Integer EX_CLIENT_FORBIDDEN_CODE = 40331;
	public static final Integer EX_OTHER_CODE = 500;
	public static final String CONTEXT_KEY_USER_ID = "currentUserId";
	public static final String CONTEXT_KEY_USERNAME = "currentUserName";
	public static final String CONTEXT_KEY_USER_NAME = "currentUser";
	public static final String CONTEXT_KEY_USER_TOKEN = "currentUserToken";
	public static final String JWT_KEY_USER_ID = "userId";
	public static final String JWT_KEY_NAME = "name";
	// 请求异常
	public static final Integer STAFF_INVALID_CODE = 2001;
	public static final Integer INSERT_LEAVE_ERROR_CODE = 2002;
	public static final Integer INSERT_HOLIDAY_WORK_ERROR_CODE = 2003;
	public static final Integer LEAVE_DAYS_EXCESS_ERROR_CODE = 2004;
	public static final Integer HOLIDAY_WORK_EXCESS_ERROR_CODE = 2005;
}
